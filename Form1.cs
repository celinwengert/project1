using System.Security.Policy;

namespace Arbeitszeit_Kalkulator
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();

            //Event-Handler für die TextChanged-Ereignisse hinzu
            textBox1.TextChanged += BerechneMorgengesamt;
            textBox2.TextChanged += BerechneMorgengesamt;
            textBox3.TextChanged += BerechneNachmittaggewünscht;
            textBox4.TextChanged += BerechneNachmittaggewünscht;

            this.Text = "ARBEITSZEITRECHNER";
        }



        private void BerechneMorgengesamt(object? sender, EventArgs e)
        {
            if (DateTime.TryParse(textBox1.Text, out DateTime morgenStart) &&
                DateTime.TryParse(textBox2.Text, out DateTime morgenEnde))
            {
                TimeSpan gesamtMorgenZeit = morgenEnde - morgenStart;
                label1.Text = gesamtMorgenZeit.ToString("h\\:mm");

                // Berechnen und aktualisieren Sie die Dezimalzahl für die Morgenzeit
                double gesamtMorgenDezimal = gesamtMorgenZeit.TotalHours;
                label3.Text = gesamtMorgenDezimal.ToString("N2");

                BerechneGesamtArbeitszeit();
            }
        }



        private void BerechneNachmittaggewünscht(object? sender, EventArgs e)
        {
            if (DateTime.TryParse(textBox3.Text, out DateTime nachmittagStart))
            {
                // Die gewünschte Arbeitszeit (8 Stunden und 24 Minuten) in TimeSpan umwandeln
                TimeSpan gewünschteArbeitszeit = new TimeSpan(8, 24, 0);



                // Umwandeln der DateTime in TimeSpan
                TimeSpan nachmittagStartZeitSpan = nachmittagStart.TimeOfDay;



                // Berechnen Sie die verbleibende Zeit für den Nachmittag
                TimeSpan verbleibendeZeit = gewünschteArbeitszeit - TimeSpan.Parse(label1.Text);



                if (verbleibendeZeit < TimeSpan.Zero)
                {
                    // Wenn die verbleibende Zeit negativ ist, setzen Sie die "endeNachmittagBox" auf "Zeit überschritten"
                    textBox4.Text = "Zeit überschritten";
                }
                else
                {
                    // Berechnen und aktualisieren Sie die Endzeit am Nachmittag
                    TimeSpan endeNachmittag = nachmittagStartZeitSpan + verbleibendeZeit;
                    DateTime endeNachmittagDateTime = DateTime.Today + endeNachmittag;
                    textBox4.Text = endeNachmittagDateTime.ToString("HH:mm");
                }



                // Berechnen und aktualisieren Sie die Zeit für den Nachmittag
                TimeSpan gesamtNachmittagZeit = TimeSpan.Parse(textBox4.Text) - nachmittagStartZeitSpan;
                label2.Text = gesamtNachmittagZeit.ToString("h\\:mm");



                // Berechnen und aktualisieren Sie die Dezimalzahl für die Nachmittagszeit
                double gesamtNachmittagDezimal = gesamtNachmittagZeit.TotalHours;
                label4.Text = gesamtNachmittagDezimal.ToString("N2");



                BerechneGesamtArbeitszeit();
            }
        }



        private void BerechneGesamtArbeitszeit()
        {
            if (TimeSpan.TryParse(label1.Text, out TimeSpan morgenZeit) &&
                TimeSpan.TryParse(label2.Text, out TimeSpan nachmittagZeit))
            {
                TimeSpan gesamtArbeitszeitBox = morgenZeit + nachmittagZeit;
                label6.Text = gesamtArbeitszeitBox.ToString("h\\:mm");



                // Berechnen und aktualisieren Sie die Dezimalzahl für die gesamte Arbeitszeit
                double gesamtArbeitszeitDezimal = gesamtArbeitszeitBox.TotalHours;
                label5.Text = gesamtArbeitszeitDezimal.ToString("N2");



            }
        }
    }
}